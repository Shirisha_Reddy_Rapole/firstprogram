package com.example.s530484.assignment1;

import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void reverseFunction(View v)
    {
        EditText inputText=(EditText)findViewById(R.id.inputText);
        String in=inputText.getText().toString();
        String reverse="";
        for ( int i = in.length() - 1 ; i >= 0 ; i-- )
            reverse = reverse + in.charAt(i);
        inputText.setText(reverse);
    }

    public void colorRed(View view)
    {
        EditText colorText=(EditText)findViewById(R.id.colorText);
        colorText.setTextColor(Color.RED);

    }
    public void colorBlue(View view)
    {
        EditText colorText=(EditText)findViewById(R.id.colorText);
        colorText.setTextColor(Color.BLUE);

    }
    public void colorGreen(View view)
    {
        EditText colorText=(EditText)findViewById(R.id.colorText);
        colorText.setTextColor(Color.GREEN);

    }
    public void unicodeFunction(View view)
    {
        EditText colorText=(EditText)findViewById(R.id.colorText);
        String console=colorText.getText().toString();

        char uni='\u2744';
        String result=uni+" ";
        console=console.toUpperCase();

        for(int i=0;i<console.length();i++)
        {
            result+=console.charAt(i)+" "+uni+" ";
        }
        colorText.setText(result.toString());
    }

}

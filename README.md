##Goal: Build a simple app with an image, editable text, plain text, and a button.

###Requirements:
The image should be one that includes you. The image should cover the top
1/2 of the screen.  
Just below the image will be the text view component. It will span the screen
and should start with your nickname.  
There should be an editable plain text in the screen below your name.  
The button should be just to the right of the editable text component.  
The button will have an onClick action that take the string from the editable
text component and reverses the order of the words and the words
themselves. For example: If the text is "One Two Three", it should change to
" eerhT owT enO ". If the text is "Hello", it should change to "olleH".  

###Bonus:
• Change the properties (Size, Color, Font) of the text.
• Add Unicode characters to the text
• Change the app name.
• Add extra buttons and actions.
• Add in another editable plain text and change the text in a different way.